---
sidebar_position: 10
title: Email
---

import styles from '../styles.module.css';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';





# Installation d'une feature Symfony

:::note précisions

Dans ce tuto, nous allons vous expliquer comment envoyer des mails avec Mailer

:::

## Mailer

![nyanCat](https://f.hellowork.com/blogdumoderateur/2013/02/nyan-cat-gif-1.gif)

:::note précisions

Le but est d'envoyer un Email à chaque action réalisée sur nos tickets (Ajout, suppression, modification).

:::

## Requirements

Afin de pouvoir mettre en place Mailer vous devez vous assurer de réaliser les étapes suivantes :

### Pré-requis

- IDE (VSCode ou PHPStorm recommandé)
- Terminal (iTerm2 sous macOS, Hyper sous Windows recommandé)
- Versionning (Git) <span className="red-text">fortement recommandé</span>
- PHP 7.2.5 minimum (Symfony 5, PHP 8.0.2 -> Symfony 6)
- MySQL
- Composer (Gestionnaire de dépendances PHP)
- Symfony CLI
- Le pack "Mailer"

:::caution vérification
Vérifier ensuite la bonne installation de l'ensemble des composants

```bash
  symfony check:requirements
```

:::


## Installation de Mailler

Se déplacer dans le répertoire de travail à l'aide de la ligne de commande

<Tabs groupId="operating-systems">
  <TabItem value="new" label="Composer">

Ajouter Mailer en utilisant composer

```bash

composer require symfony/mailer

```
</TabItem>
</Tabs>

## Mise en place de MailTrap

- Créez un compte sur [MailTrap](https://www.mailtrap.io)
- Une fois le compte crée, dans le menu rendez vous sur l'onglet Inboxes
- ![Arbo](../img/arborescenceMailTrap.png)
- Choisissez l'intégration Symfony 5+
- ![Integration](../img/mailerURL.png)
- et Copiez le code MAILER_DSN dans le fichier .env


```yaml

# In all environments, the following files are loaded if they exist,
# the latter taking precedence over the former:
#
#  * .env                contains default values for the environment variables needed by the app
#  * .env.local          uncommitted file with local overrides
#  * .env.$APP_ENV       committed environment-specific defaults
#  * .env.$APP_ENV.local uncommitted environment-specific overrides
#
# Real environment variables win over .env files.
#
# DO NOT DEFINE PRODUCTION SECRETS IN THIS FILE NOR IN ANY OTHER COMMITTED FILES.
#
# Run "composer dump-env prod" to compile .env files for production use (requires symfony/flex >=1.2).
# https://symfony.com/doc/current/best_practices.html#use-environment-variables-for-infrastructure-configuration

###> symfony/framework-bundle ###
APP_ENV=prod
APP_SECRET=f41b4cafd50223c7b11e2e76dedd37f6
###< symfony/framework-bundle ###

###> doctrine/doctrine-bundle ###
# Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# IMPORTANT: You MUST configure your server version, either here or in config/packages/doctrine.yaml
#
# DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
# DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7&charset=utf8mb4"
DATABASE_URL="postgresql://symfony:ChangeMe@127.0.0.1:5432/app?serverVersion=13&charset=utf8"
###< doctrine/doctrine-bundle ###

###> symfony/mailer ###
//This will success
MAILER_DSN=smtp://1ea724ebbac063:977525dc0496e4@smtp.mailtrap.io:2525?encryption=tls&auth_mode=login
//This will error
# MAILER_DSN=NULL//NULL
###< symfony/mailer ###


```

## Création du controller


- Créez dans le dossier Controller, un fichier "MailerController.php"
- Copiez le code suivant dans le fichier "MailerController.php"

``` php

<?php 
// src/Controller/MailerController.php
namespace App\Controller;

use App\Entity\Ticket;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class MailerController extends AbstractController
{
    # [Route('/email')]
    public static function sendEmail(MailerInterface $mailer, string $mailTo, string $subject, string $text, Ticket $ticket) : void
    { 

        $email = (new TemplatedEmail())
            ->from('hello@example.com')
            ->to($mailTo)
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject($subject)
            ->text($text)
            ->htmlTemplate('email/emailClient.html.twig')
            ->context(['text'=>$text, 'ticket' =>$ticket]);

        $mailer->send($email);

        // ...
    }
}

```


### Création des templates Twig

- Créez un dossier "email" dans le dossier "templates"
- Créez le fichier "emailClient.html.twig"
- Copiez le code suivant :

```html 

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>
			{% block title %}Welcome!
			{% endblock %}
		</title>
		<link rel="icon" href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 128 128%22><text y=%221.2em%22 font-size=%2296%22>⚫️</text></svg>">
		<link href="https://bootswatch.com/5/lux/bootstrap.min.css" rel="stylesheet">
		{% block stylesheets %}
			{{ encore_entry_link_tags('app') }}
		{% endblock %}

	</head>
	<body>
	

		<style>

			h1 {
				color: red;
			}

            .numTicket {
                font-size : 3vh;
                color : blue;
            }

		</style>


		<container>
			<row class="header">
				<h1>Bonjour
					{{ app.user.name }}
					!</h1>
			</row>
		</container>

		<p class="">
			Le ticket n°<span class="numTicket">{{ ticket.id }}</span>
			contenant l'objet :
			<span class="numTicket"> {{ticket.object}} </span>
			{{ text }}</p>


        <br><br><br>
        <p>L'équipe Technique vous souhaite une bonne journée.
        <br><br>
        Cordialement,
        
        </p>



```

# Mettre en place l'envoi d'un mail dans un controller :

Au sein de votre fonction, mettez le code suivant :

```php
MailerController::sendEmail($mailer, "adresse du destinataire", "Motif de l'envoi du mail", "texte complémentaire", $ticket);

```

## Modifier le TicketController.php

### Pour la suppresion d'un ticket :
```php
    /**
     * * @Route("/delete/{id}", name="ticket_delete", requirements={"id"="\d+"})
     */
    public function deleteTicket(Ticket $ticket, MailerInterface $mailer): Response
    {
        //This will success
        MailerController::sendEmail($mailer, "user1@test.fr", "Ticket Supprimé", " a bien été supprimé", $ticket);
        
        $this->ticketRepository->remove($ticket, true);

        $this->addFlash(
            'warning',
            'Votre ticket a bien été supprimé');

        return $this->redirectToRoute('app_ticket');
    }

```
### Pour l'ajout et la modification d'un ticket :

```php

    public function ticket(Ticket $ticket = null,
                        Request $request,
                        //This will success
                        MailerInterface $mailer)
    {
        if (!$ticket) {
        $ticket = new Ticket;

        $ticket->setIsActive(true)
            ->setCreateAt(new \DateTimeImmutable());
        
        $title = 'Création d\'un ticket';
        }else {
            $title = "Update du formulaire : {$ticket->getId()}";

        }
        
        $form = $this->createForm(TicketType::class, $ticket, []);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->ticketRepository->add($ticket, true);
//This will success
            if ($title == 'Création d\'un ticket') {
                //This will success
                MailerController::sendEmail($mailer, "user1@test.fr", "Ticket ajouté", " a bien été ajouté", $ticket);
                $this->addFlash(
                    'success',
                    'Votre ticket a bien été ajouté');
                    
//This will success
            } else {
                //This will success
                MailerController::sendEmail($mailer, "user1@test.fr", "Ticket modifié", " a bien été modifié", $ticket);
                $this->addFlash(
                    'info',
                    'Votre ticket a bien été mis à jour');
            }

            return $this->redirectToRoute('app_ticket');
        }
        return $this->render("ticket/userForm.html.twig", [
            "form" => $form->createView(),
            'title' => $title
        ]);
    }

```


## Résultat dans l'onglet Inboxes de MailTrap

![Email](../img/exempleMail.png)


# <span className="red-text">Ce tutoriel vous a été présenté par l'Équipe Nicolas et Maxime d'un MaxDeFormation.com</span>