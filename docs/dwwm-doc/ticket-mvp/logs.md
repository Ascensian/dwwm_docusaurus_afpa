---
sidebar_position: 7
title: Logs
---

# Logs - Journalisation


@Author Hugo, Romuald

# INSTALLATION

Dans cet exemple nous allons afficher les logs concernant les informations de l'utilisateur en cours :

**MODE DEV :** 

**1.**

D'abord se mettre en mode _développeur_ dans le fichier **.env.local**, repérez la ligne avec APP_ENV et mettez _dev_

`APP_ENV=dev`

**2.**

Ensuite placez vous dans le fichier TicketController.php,

- Importez l'interface LoggerInterface
`use Psr\Log\LoggerInterface;`

- Déclarez une variable de type protected
- Ajoutez dans le constructeur un paramètre de type _LoggerInterface_ puis assigner sa valeur à la variable

**3.**

Toujours dans le fichier TicketController.php, récupérez la valeur des informations de l'utilisateur à l'endroit ou vous voulez controler l'action des logs, par exemple pour l'affichage des tickets vous mettez les lignes ci dessous dans la méthode index du controller

```php
$userMail = $this->getUser()->getUserIdentifier();
$userPwd = $this->getUser()->getPassword();
$userRole = $this->getUser()->getRoles();
```

Puis à partir de ces informations, vous les transmettez grâce à l'objet LoggerInterface contenu dans la variable déclarée précédemment

```php
$this->log->info('EMAIL', array($userMail));
$this->log->info('PASSWORD', array($userPwd));
$this->log->info('ROLE', array($userRole));
```

_**Si vous avez des problèmes à ce stade, mettez la commande suivante dans le terminal, si vous n'en avez pas mettez la quand même nous en aurons besoin plus tard**_

> **composer require symfony/monolog-bundle**

Après avoir lancé la commande, dans votre dossier `config/packages/` vous devriez voir un fichier monolog.yaml

**4.**

Ouvrez votre fichier monolog.yaml,

- Créer un channel qui servira à trier les informations récupérées de l'utilisateur, placez la ligne suivante en dessous de la première ligne monolog
```php 
channels: ['userInfos']
```
- Ensuite supprimer le handler _main_ et tous ses attributs, puis remplacez par ce handler personnalisé

```php
user:
                type: rotating_file
                path: "%kernel.logs_dir%/info_user.log"
                level: info
                channels: ["userInfos"]
                max_files: 10
                
syslog_handler:
                type: syslog
                # log error-level messages and higher
                level: info
```
Explications des lignes :

- `type: rotating_file` : permet l'affichage dynamique d'un fichier de log dans le dossier `var/log/` 
- `path: "%kernel.logs_dir%/info_user.log"` : spécifie le chemin et le fichier dans lesquels les informations doivent s'écrire
- `level: info` : filtre le niveau des logs, n'affichera que les logs info et ceux de niveau inférieur
- `channels: ["userInfos"]` : permet de trier les logs pour ne prendre que les informations récupérées
- `max_files: 10` : controle le nombre de fichier généré par le `type: rotating_file`

**5.**

Rafraichir la page d'index des tickets, et regardez le dossier `var/log`, normalement vous devez voir un fichier généré automatiquement qui contient la liste des logs de l'utilisateur. 

_Ce que vous devez voir_

```php 
[2022-05-30T11:59:12.617468+00:00] userInfos.INFO: EMAIL ["admin@test.test"] []
[2022-05-30T11:59:12.619476+00:00] userInfos.INFO: PASSWORD ["$2y$13$STvX9cQ5ppRhsvU9qKDqX.4g68ClmtcZ9DDj9XKuwKJuurSSM0tT6"] []
[2022-05-30T11:59:12.620443+00:00] userInfos.INFO: ROLE [["ROLE_ADMIN","ROLE_USER"]] []
```

!!! Si aucun fichier n'apparait dans le dossier `var/log` faites la commande suivante

> **composer install**

