---
sidebar_position: 13
title: Workflow
---

# <span className= "titleSize">**<span className= "colorB">W</span><span className= "colorR">o</span><span className= "colorG">r</span><span className= "colorY">k</span><span className= "colorP">f</span><span className= "colorV">l</span><span className= "colorG">o</span><span className= "colorR">w</span>**</span>

## <span className= "colorPink">**Accrochez vos ceintures,**</span>

## <span className= "colorPink">**Vous pénétrez dans un nouvel univers !**</span>


# <span className= "colorB">**1er saut hyper spatial**</span>


Les effets de l'hyper-espace s'estompent, il faut passer aux choses sérieuses.
Tout d'abord un peu d'installation.
Dans la console:

`
composer require workflow
`

> Et voilà c'est gagné !

Vous venez de générer des fichiers dont un **workflow.yaml**
C'est sur celui-ci que nous allons travailler.

```yaml
framework:
  workflows:
    ticketTraitement:
      type: "state_machine" # or 'workflow'
      audit_trail:
        enabled: true
      marking_store:
        type: "method"
        property: "ticketStatut" # on va modifier la BDD en conséquence
      supports:
        - App\Entity\Ticket
      initial_marking: initial
      places:
        - initial
        - wip
        - clientAcceptance
        - finished
      transitions:
        to_wip:
          from: initial
          to: wip
        to_client:
          from: wip
          to: clientAcceptance
        to_finished:
          from: clientAcceptance
          to: finished
```

### <span className= "colorG">**ticketTraitement :**</span>

correspond au nom du workflow,
on aurait pu mettre n'importe quoi tant que ça reste explicite, comme :

> workflowChrChr

Le Type peut être "state_machine" ou "workflow".
Dans un workflow, une entité peut avoir plusieurs états à la fois,
contraitement au state_machine.

### <span className= "colorG">**property :**</span>

désigne l'attribut de l'entité, sa colonne en BDD.

### <span className= "colorG">**supports :**</span>

désigne l'entité concernée par le workflow.

### <span className= "colorG">**initial_marking :**</span>

représente la valeur de l'état initial,
On peut mettre la valeur que l'on veut, _c'est une chienne de caractère_.

### <span className= "colorG">**places :**</span>

Liste les différentes valeurs possibles,
ce sont aussi des _chiennes de caractères_.

### <span className= "colorG">**transitions :**</span>

Définit le changement d'état,
ce sont les noms qui seront appelés pour passer d'un état à un autre.


# <span className= "colorB">**C'est bien mais ça donne quoi ?**</span>

> JE NE CROIS QUE CE QUE JE VOIS - _un mec malin_

On peut avoir une représentation visuelle du workflow construit.
2 méthodes possibles suivant votre machine.

Vous avez installé **Node.js** avec chocolatey. 
Vous pouvez installer **graphviz**

`choco install graphviz`

Et de là, toujours dans la console:

`symfony console workflow:dump ticketTraitement | dot -Tpng -o graph.png`

vous avez alors créé un fichier graph.png à la racine du projet.
```mermaid
graph LR
initial0(["initial"])
wip1(("wip"))
clientAcceptance2(("clientAcceptance"))     
finished3(("finished"))
initial0-->|"to_wip"|wip1
wip1-->|"to_client"|clientAcceptance2       
wip1-->|"to_finished"|finished3
clientAcceptance2-->|"to_finished"|finished3
```

Si vous êtes vierge de tout node, tout espoir n'est pas perdu.
On retourne dans la console, cette fois avec la commande:

`symfony console workflow:dump ticketTraitement --dump-format=mermaid`

Il suffit de se rendre dans un éditeur comme **https://stackedit.io**



Placez le code dans une balise :

` ```mermaid`

```
graph LR

initial0(["initial"])

wip1(("wip"))

clientAcceptance2(("clientAcceptance"))

finished3(("finished"))

initial0-->|"to_wip"|wip1

wip1-->|"to_client"|clientAcceptance2

clientAcceptance2-->|"to_finished"|finished3
```

` ``` `

pensez à fermer la balise


# <span className= "colorB">**2ème saut hyper-spatial**</span>

On a un workflow, c'est bien.
Il s'agirait de s'en servir peut être...

On va modifier le système de statut du ticket.
Donc un peu de BDD pour se mettre bien.

On va modifier le nom de la colonne isActive, pour mieux suivre le workflow!
On va donc modifier son type en **ENUM**, on aura une liste de valeurs pour notre colonne _ticket_statut_.

Suivez le guide :

>On chemine dans src/Entity/Ticket.php 

et on remplace 
```php

/**
 * @ORM\Column(type="boolean")
 */
private $isActive;

```

par :


```php
/**
 * 
 * @ORM\Column(type="string", columnDefinition="ENUM('initial','wip','clientAcceptance','finished')")
 */
private $ticket_statut;
```

Et pour activer la modification en BDD :

`
symfony console doctrine:schema:update --dump-sql
`

## <span className= "colorPink">**Maintenant que tout est cassé ?**</span>

Pour que le type **ENUM** soit pris en compte, on aller prévenir doctrine de ce qu'on a fait.

>Dans config/packages/doctrine.yaml

On ajoute ceci :

```yaml
doctrine:
    dbal:
        url: '%env(resolve:DATABASE_URL)%'
        mapping_types:
            enum: string
```

Avec notre chère AppFixtures, on va tout remettre en ordre de marche pour le flow.

is_active n'existant plus, on va modifier ce qu'il y a dans nos tickets.

Pour le class Ticket, le ternaire devient alors :

```php
setFinished($ticket->getTicketStatut()!='finished' ? ImmutableDateTime::immutableDateTimeBetween('now', '6 months') : null)
```


# <span className= "colorB">**3ème saut hyper-spatial**</span>

Désormais tout est paré pour balancer un gros flow des familles!

Maintenant qu'on a la logique, intégrons tout ça dans l'application. Et modifier les statuts du ticket suivant la situation.

## <span className= "colorG">**D'abord reprendre le contrôle :**</span>

Dans le **TicketController** et plus précisément la method _index()_ nous allons faire des modifications.

- Si c'est une création de ticket(dans le _if_) : donner la valeur _'initial'_

```php
$ticket->setTicketStatut("initial")
    ->setCreatedAt(new \DateTimeImmutable());
```
- Si c'est une modification de ticket (dans le _else_) : appliquer la transition _'to_wip'_

```php
$workflow = $this->registry->get($ticket, 'ticketTraitement');
$workflow->apply($ticket, 'to_wip');
```

Et pour prendre en compte l'affichage de nos camemberts enviés partout à travers le monde, les fonctions créées sont désormais obsolètes.active

On va alors récupérer le nombres de tickets en statut _initial_ et en statut _finished_

```php
$countActiveTicket = count($this->ticketRepository->findBy(['ticket_statut' => 'initial' ]));
$countNoActiveTicket = count($this->ticketRepository->findBy(['ticket_statut' => 'finished' ]));
```

Ajoutons une fonction permettant de finir un ticket :
```php
/** 
 * @Route("/close/{id}", name="ticket_close",requirements={"id"="\d+"})
 */
public function closeTicket(Ticket $ticket): Response
{
    $workflow = $this->registry->get($ticket, 'ticketTraitement');
    $workflow->apply($ticket, 'to_finished');
    $this->ticketRepository->add($ticket, true);

    return $this->redirectToRoute('app_ticket');
}
```

## <span className= "colorG">**Maintenant on peut danser le TWIG :**</span>

On se fait beau, on peigne sa double moustache en modifiant le contenu comme suit :

```twig
{{ticket.isActive}}
```
devient : 
```twig
{{ticket.TicketStatut}}
```

Les boutons vont désormais danser au rythme du statut du ticket :

```php
<a href="{{path('ticket_update',{'id' :ticket.id})}}" class="btn btn-success">
        <em class="fa-solid fa-gears"></em>
    </a>
    {% if ticket.ticketStatut == 'finished' %}
        <a href="{{path('ticket_delete',{'id' :ticket.id})}}" class="btn btn-danger">
            <i class="fa-solid fa-trash-can"></i>
        </a>
    {% endif %}

    {% if ticket.ticketStatut == 'wip' %}
        <a href="{{path('ticket_close',{'id' :ticket.id})}}" class="btn btn-warning">
            <em class="fa-solid fa-square-xmark"></em>
        </a>
    {% endif %}
```

@Author Marin, Arnaud
