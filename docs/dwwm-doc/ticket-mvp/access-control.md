---
sidebar_position: 1
title: Access Control
---

import styles from '../styles.module.css';

# Access Control + Propriétaire Tickets

@Author Vanessa, Thibault

## Pré-requis

- IDE (VSCode ou PHPStorm recommandé)
- Terminal (iTerm2 sous macOS, Hyper sous Windows recommandé)
- Versionning (Git) fortement recommandé
- PHP 7.2.5 minimum (Symfony 5, PHP 8.0.2 -> Symfony 6)
- MySQL
- Composer (Gestionnaire de dépendances PHP)
- Symfony CLI

### Bundles nécessaires

- DoctrineBundle
- DoctrineFixturesBundle
  <br />
  <br />

## Modification de l’entity « Ticket »

<p>Afin de pouvoir filtrer les tickets par "auteur", il est nécessaire d'ajouter une relation entre les entités Ticket et User.</p>

```bash
symfony console make :entity Ticket
```
> Your entity already exists! So let's add some new fields!
> <span className="green">New property name (press 'return' to stop adding fields)</span>:<br />
> user<br /> <span className="green">Field type (enter <span className="yellow">?</span> to see all types) [<span className="yellow">string</span>]</span>:<br /> relation<br /> <span className="green">What class should this entity be related to?</span>:<br />
> User<br />
> What type of relationship is this?<br /> <span className="green">Relation type? [ManyToOne, OneToMany, ManyToMany, OneToOne]</span>:<br /> ManyToOne<br /> <span className="green">Is the <span className="yellow">Ticket.user</span> property allowed to be null (nullable)? (yes/no) <span className="yellow">[yes]</span></span>:<br /> no<br /> <span className="green">Do you want to add a new property to <span className="yellow">User</span> so that you can access/update <span className="yellow">Ticket</span> objects from it - e.g. <span className="yellow">$user->getTickets()</span>? (yes/no) <span className="yellow">[yes]</span></span>:<br />
> yes<br />
> A new property will also be added to the <span className="yellow">User</span> class so that you can access the related <span className="yellow">Ticket</span> obects from it.<br /> <span className="green">New field name inside User <span className="yellow">[tickets]</span></span><br /> <br />
> Do you want to activate <span className="yellow">orphanRemoval</span> on your relationship?<br />
> A <span className="yellow">Ticket</span> is 'orphaned' when it is removed from its related <span className="yellow">User</span>.<br /> e.g. <span className="yellow">$user->removeTicket($ticket)</span><br /><br />
> NOTE: If a <span className="yellow">Ticket</span> may \*change\* form one <span className="yellow">User</span> to another, answer "no".<br /><br /> <span className="green">Do you want to automatically delete orphaned <span className="yellow">App\Entity\Ticket</span> objects (orphanRemoval)? (yes/no)</span> <span className="yellow">[no]</span>:<br /> yes <br /><br /> <span className="yellow">updated:</span> src/Entity/Ticket.php<br /><br /> <span className="green">Add antother property? Enter the property name (or press 'return' to stop adding fields)</span>:<br /><br /> <span className="background-green">Success !</span> <br /> <br />

## Générer la migration

```bash
 symfony console make:migration
```

<br />

## Modification avant lancement de la migration

Dans le dernier fichier de migration,
ajout de la ligne ci-dessous dans la méthode : `public function up()`

```php
$this->addSql('TRUNCATE TABLE ticket') ;
```

<br />

## Lancer la migration

```bash
symfony console doctrine:migration:migrate --no-interaction
```

<br />

## Création de fixtures

Modifier le fichier AppFixtures :
- en plus des données département,
- on récupère toutes les données de tous les users

```php
$allUsers = $manager->getRepository(User::class)->findAll();
```

pour ensuite nourrir le ticket on lui ajoute un user

```php
 ->setUser($faker->randomElement($allUsers));
```

## Importation des fixtures

```bash
symfony console doctrine:fixtures:load --no-interaction
```

# Affichage des tickets de l'utilisateur connecté

## Récupération des informations user

Dans le fichier TicketController.php > `function index()`

Décommenter ou ajouter la ligne :

```php
// **_récupère les infos de l’utilisateur en cours_**
$user = $this->getUser();
```

<br />Modifier la ligne de récupération de tous les tickets par :

```php
// **_récupère les tickets là où l’utilisateur existe_**
$tickets = $this->ticketRepository->findBy(['user' => $user] );
```
Toujours dans le fichier TicketController.php
Pour éviter les soucis de création de ticket il faut également ajouter la ligne suivante dans la `function ticket()` 

```php
$user = $this->getUser();
```

puis nourrir le ticket avec un user 
```php
->setIdUser($user)
```

### Vérifiez, sur votre page d’accueil après connexion, vous ne devriez avoir que les tickets qui ont été créé par l’utilisateur connecté.
