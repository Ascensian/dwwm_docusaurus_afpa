---
sidebar_position: 3
title: Structure Documentation
---

import styles from './styles.module.css';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import BrowserWindow from '@site/src/components/BrowserWindow';

> Exemple de documentation détaillée.
>
> Vous devez pour chaque procédure, détailler précisément les différentes étapes et action à effectuer afin d'atteindre le résultat escompté.
> Pour cela utilisé la syntaxe Markdown pour générer une page claire et précise.

# Contexte

![symfony](./img/symfony_black_02.png#gh-light-mode-only)![symfony](./img/symfony_white_02.png#gh-dark-mode-only)<br />
Installation d'une application Symfony dans le cadre de la découverte du framework Symfony

:::note précisions

Expliquez la demande et le contexte initial avec le but à atteindre.

:::

## Requirements

Afin de pouvoir développer en utilisant le framework Symfony vous devez vous assurer d'avoir les pré-requis suivant

### Pré-requis

- IDE (VSCode ou PHPStorm recommandé)
- Terminal (iTerm2 sous macOS, Hyper sous Windows recommandé)
- Versionning (Git) <span className="red-text">fortement recommandé</span>
- PHP 7.2.5 minimum (Symfony 5, PHP 8.0.2 -> Symfony 6)
- MySQL
- Composer (Gestionnaire de dépendances PHP)
- Symfony CLI

:::caution vérification
Vérifier ensuite la bonne installation de l'ensemble des composants

```bash
  symfony check:requirements
```

:::

### Extensions VSCode Utiles

Afin de vous faciliter le développement d'une application Symfony, assurez-vous d'avoir activé les extensions suivantes :

<Tabs>
  <TabItem value="docblock" attributes={{className: [styles.vscode, styles.ico_tab] }} label="PHP DocBlocker" default>
    Permet de générer rapidement des documentations pour les fonctions que l’on crée
  </TabItem>
  <TabItem value="twig" attributes={{className: [styles.vscode, styles.ico_tab] }} label="Twig Language 2">
    Permet la coloration syntaxique au sein des fichiers .twig
  </TabItem>
  <TabItem value="namespace" attributes={{className: [styles.vscode, styles.ico_tab] }} label="PHP Namespace Resolver">
    Essentielle lorsqu’on travaille avec le systèmes des espaces de noms de PHP, elle vous permettra de retrouver aisément dans quel espace de noms se trouve telle ou telle classe de Symfony
  </TabItem>
  <TabItem value="gitgraph" attributes={{className: [styles.vscode, styles.ico_tab] }} label="Git Graph">
    Permet de visualiser et naviguer dans les commits au travers d’une interface visuelle
  </TabItem>
</Tabs>

## Création du projet

Se déplacer dans le répertoire de travail à l'aide de la ligne de commande

<Tabs groupId="operating-systems">
  <TabItem value="new" label="Symfony CLI">

Créer notre application Symfony en utilisant le CLI

```bash
# run this if you are building a traditional web application
symfony new my_project_name --webapp

# run this if you are building a microservice, console application or API
symfony new my_project_name
```

</TabItem>
<TabItem value="old" label="Composer">

Créer notre application Symfony en utilisant Composer

```bash
# run this if you are building a traditional web application
composer create-project symfony/skeleton my_project_directory
cd my_project_directory
composer require webapp

# run this if you are building a microservice, console application or API
composer create-project symfony/skeleton my_project_directory
```

</TabItem>
</Tabs>

### Architecture classique d'une application Symfony

<BrowserWindow padding="0" url="http://localhost:8000">

```tree
📦application_symfony
 ┣ 📂bin
 ┃ ┗ 📜console
 ┣ 📂config
 ┣ 📂migrations
 ┣ 📂public
 ┃ ┣ 📜.htaccess
 ┃ ┗ 📜index.php
 ┣ 📂src
 ┃ ┣ 📂Controller
 ┃ ┣ 📂Entity
 ┃ ┣ 📂Repository
 ┃ ┗ 📜Kernel.php
 ┣ 📂templates
 ┃ ┗ 📜base.html.twig
 ┣ 📂var
 ┣ 📂vendor
 ┣ 📜.env
 ┣ 📜.env.local
 ┣ 📜.gitignore
 ┣ 📜composer.json
 ┣ 📜composer.lock
 ┗ 📜symfony.lock
```

</BrowserWindow>

<Tabs>
  <TabItem value="bin" attributes={{className: [styles.bin, styles.ico_tab] }} label="bin" default>
    Contient le principal point d’entrée de la ligne de commande : console.<br />
    Vous l’utiliserez tout le temps.<br />
  </TabItem>
  <TabItem value="config" attributes={{className: [styles.config, styles.ico_tab] }} label="config">
    Constitué d’un ensemble de fichiers de configurations sensibles, initialisés avec des valeurs par défaut. Un fichier par paquet.<br />
    Vous les modifierez rarement : faire confiance aux valeurs par défaut  est presque toujours une bonne idée ;)
  </TabItem>
  <TabItem value="public" attributes={{className: [styles.public, styles.ico_tab] }} label="public">
    Est le répertoire racine du site web, et le script index.php est le point d’entrée principal de toutes les ressources HTTP dynamiques.
  </TabItem>
  <TabItem value="src" attributes={{className: [styles.src, styles.ico_tab] }} label="src">
    Héberge tout le code que vous allez écrire.<br />
    C’est ici que vous passerez la plupart de votre temps.<br />
    Par défaut, toutes les classes de ce répertoire utilisent le namespace PHP App.<br />
    C’est votre répertoire de travail, votre code, votre logique de domaine.
  </TabItem>
  <TabItem value="templates" attributes={{className: [styles.templates, styles.ico_tab] }} label="templates">
    Héberge tous les templates html.twig que vous allez écrire.<br />
    C’est le répertoire pour gérer votre visuel.<br />
    (⚠️ CSS et JS sont à placer dans le répertoire public/)
  </TabItem>
  <TabItem value="var" attributes={{className: [styles.var, styles.ico_tab] }} label="var">
    Contient les caches, les logs et les fichiers générés par l’application lors de son exécution.<br />
    Vous pouvez le laisser tranquille.<br />
    (Répertoire à supprimer si vous souhaiter faire une copie de votre application)
  </TabItem>
  <TabItem value="vendor" attributes={{className: [styles.vendor, styles.ico_tab] }} label="vendor">
    Contient tous les paquets installés par Composer, y compris Symfony lui-même.<br />
    C’est notre arme secrète pour un maximum de productivité.<br />
    Ne réinventons pas la roue. Vous profiterez des bibliothèques existantes pour vous faciliter le travail.<br />
    Le répertoire est géré par Composer.<br />
    <span className="red-text">N’y touchez jamais. (si vous devez modifier le comportement natif de Symfony créez une extension)</span><br />
    (Répertoire à supprimer si vous souhaiter faire une copie de votre application)
  </TabItem>
  <TabItem value="env" attributes={{className: [styles.env, styles.ico_tab] }} label=".env">
    Fichier de configuration d’environnement de votre application Symfony
  </TabItem>
  <TabItem value="composer" attributes={{className: [styles.composer, styles.ico_tab] }} label="composer.json">
    Liste les bundles installés dans votre application.<br />
    Vous pouvez désinstaller un bundle en le supprimant de ce fichier puis en lançant la commande composer update
  </TabItem>
</Tabs>

## Installation des bundles nécessaires

Nous avons besoin d'installer des bundles afin d'ajouter des fonctionnalités importantes à notre application Symfony.

### Doctrine

Doctrine est l'un des bundles si ce n'est **LE** bundle le plus important de l'écho-système Symfony.
Il permet de faire le lien entre la base de donnée et l'application back-end Symfony.
Il agit comme un traducteur d'instruction SQL et cast d'objets.

```bash
composer require symfony/orm-pack
```

Suite à l'installation de ce bundle il est nécessaire de modifier le fichier **.env**

```ini
###> doctrine/doctrine-bundle ###
# Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# IMPORTANT: You MUST configure your server version, either here or in config/packages/doctrine.yaml
#
# DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
// This will success
DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7&charset=utf8mb4"
// This will error
# DATABASE_URL="postgresql://symfony:ChangeMe@127.0.0.1:5432/app?serverVersion=13&charset=utf8"
###< doctrine/doctrine-bundle ###
```

Modifiez `db_user`, `db_password` et `db_name` en fonction de la configuration de votre machine

### Annotations

Installer le support des annotations

```bash
composer require annotations
```

### Maker

Installer le Maker en environnement de dev uniquement

```bash
composer require symfony/maker-bundle --dev
```

### Form

Installer les commandes supplémentaires pour la construction et gestion des formulaires

```bash
composer require symfony/form
```

### Twig

Installer le moteur de template Twig

```bash
composer require twig
```

### VarDumper

Installer le VarDumper (Super dumper avec interface visuelle plus agréable 😉) en environnement de dev uniquement

```bash
composer require symfony/var-dumper --dev
```

### Apache Pack

Installer le module de compatibilité Apache (conseillé avec les stacks locales Wamp, Mamp, Lamp, etc…)

```bash
composer require symfony/apache-pack
```

## Vérification du fonctionnement de notre application

Lancer le serveur interne de Symfony

<Tabs groupId="operating-systems">
  <TabItem value="new" label="Symfony CLI">

```bash
symfony serve
```

</TabItem>
<TabItem value="old" label="Composer">

```bash
php bin/console server:start
```

</TabItem>
</Tabs>
Vous devriez avoir l'affichage suivant dans votre navigateur à l'adresse [http://localhost:8000](http://localhost:8000):

<BrowserWindow padding="0" url="http://localhost:8000">

![welcome-sym](./img/symfony-5-welcome.png)

</BrowserWindow>

## Générer les entités

En Symfony et comme nombre d'autres Framework, la représentation d'une table en objet manipulable en back est une Entité.

Nous allons donc modéliser et envoyer les instructions nécessaires à la création de notre Base de Données et des Tables Associées.

### Créer la BDD

<Tabs groupId="operating-systems">
  <TabItem value="new" label="Symfony CLI">

```bash
symfony console doctrine:database:create

# alias raccourci
symfony console d:d:c
```

</TabItem>
<TabItem value="old" label="Composer">

```bash
php bin/console doctrine:database:create

# alias raccourci
php bin/console d:d:c
```

</TabItem>
</Tabs>

### Entité Ticket

Notre entité doit être définie ainsi

|   Field    |        Type         | Nullable |
| :--------: | :-----------------: | :------: |
|     id     | int (AutoIncrement) | `false`  |
|   object   |    string (255)     | `false`  |
|  message   |        text         | `false`  |
|  comment   |        text         |  `true`  |
|  isActive  |       boolean       | `false`  |
| createdAt  |      datetime       | `false`  |
| finishedAt |      datetime       |  `true`  |

### Utiliser le maker

<Tabs groupId="operating-systems">
  <TabItem value="new" label="Symfony CLI">

```bash
symfony console make:entity Ticket
```

</TabItem>
<TabItem value="old" label="Composer">

```bash
php bin/console make:entity Ticket
```

</TabItem>
</Tabs>

### Générer la migration

<Tabs groupId="operating-systems">
  <TabItem value="new" label="Symfony CLI">

```bash
symfony console make:migration
```

</TabItem>
<TabItem value="old" label="Composer">

```bash
php bin/console make:migration
```

</TabItem>
</Tabs>

### Lancer la migration

<Tabs groupId="operating-systems">
  <TabItem value="new" label="Symfony CLI">

```bash
symfony console doctrine:migrations:migrate

# alias raccourci sans interactions
symfony console d:m:m --no-interaction
```

</TabItem>
<TabItem value="old" label="Composer">

```bash
php bin/console doctrine:migrations:migrate

#alias raccourci sans interactions
php bin/console d:m:m --no-interaction
```

</TabItem>
</Tabs>

## Télécharger le projet final

[🗃 Télécharger le projet](./assets/project_afpa_mvp.zip)

```uml
@startuml
Bob -> Alice : hello
@enduml
```
