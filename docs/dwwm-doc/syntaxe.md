---
sidebar_position: 1
title: Syntaxe Markdown
---

# Ceci est un H1

## Ceci est un H2

### Ceci est un H3

**Texte en gras**

_Texte en italique_

Ceci est une liste :

- Lorem
- ipsum

> Ceci est un citation

:::danger Ceci est le titre du bloc !
Ceci est un blog de **danger**
:::

:::success
Ceci est un blog de **success**
:::

:::note remarque

Un peu de **contenu** avec la `syntaxe` _markdown_. Consultez [cette `api`](#).

:::

:::info

Un peu de **contenu** avec la `syntaxe` _markdown_. Consultez [cette `api`](#).

:::

:::caution attention

Un peu de **contenu** avec la `syntaxe` _markdown_. Consultez [cette `api`](#).

:::

```js
// Ceci est un extrait de code JS
const foo = {
  bar: "John Doe",
};
```

```php
// Ceci est un extrait de code PHP
$foo = ['bar' => 'John Doe'];
```
